var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/campus');

var Account = mongoose.model('Account', {
	uuid: {
		type: String,
		index: {
			unique: true
		}
	},
	firstName: String,
	lastName: String,
	mail: String,
	role: String,
	theme: String,
	school: Object,
	language: Object,
	updateAt: Date,
	ipAddress: String
});

module.exports = {
	mongoose: mongoose,
	Account: Account
}
