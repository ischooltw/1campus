process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'

const db = require('./db.js');
const config = require('./config.js');
const convert = config.convert;

const express = require('express');
const server = express();
const cors = require('cors');
const request = require('request');
const bodyParser = require('body-parser');

// session
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const sessionStore = session({
    secret: "one-campus",
    resave: true,
    saveUninitialized: false,
    cookie: {
        path: '/',
        expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
        maxAge: 7 * 24 * 60 * 60 * 1000
    },
    store: new MongoStore({
        url: "mongodb://localhost:27017/campus"
    })
});

// server
server
    .set('view engine', 'pug')
    .use(cors())
    .use(bodyParser.urlencoded({
        extended: false,
        limit: "5mb"
    }))
    .use(bodyParser.json({
        limit: "5mb"
    }))
    .use(sessionStore)
    .get('/oauth/callback', getCode)
    .get('/oauth/token', getToken)
    .get('/', home)
    .get('/logout', logout)
    .post('/logout', logout)
    .get('/session', getSession)
    .get('/account', getAccount)
    .post('/account', setAccount)
    .post('/service', sendService)
    .use(express.static('static'))
    .listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", () => {
        console.log("Server listening at", [process.env.PWD, '/express.js'].join(''), process.env.IP || "0.0.0.0", process.env.PORT || 3000);
    });

// functions
function home(req, res) {
    if (!req.session.token)
        res.redirect(`${config.rootPath}/oauth/callback`);
    else
        res.render('index', {
            baseUrl: `${req.get('X-Forwarded-Proto') || req.protocol}://${req.hostname}${config.rootPath}`,
            authServer: config.authServer,
            logoutUrl: `${config.logoutUrl}?next=${req.get('X-Forwarded-Proto') || req.protocol}://${req.hostname}${config.rootPath}`,
            loadGadget: config.loadGadget,
            coreOnly: config.coreOnly,
            languageOnly: config.languageOnly,
            rootPath: config.rootPath,
            its5Sync: JSON.stringify(config.its5Sync)
        });
}

function logout(req, res) {
    req.session.destroy();
    res.send('Well done!');
}

function getSession(req, res) {
    res.send(req.session);
}

function getAccount(req, res) {
    res.send(req.session.account);
}

function setAccount(req, res) {
    var item = {
        updateAt: new Date(),
        ipAddress: res.get["x-forwarded-for"]
    };

    if (req.body.school) {
        item.school = {
            current: req.body.school,
            items: req.session.account.school.items
        };
        req.session.account.school = item.school;
        req.session.account.role = null;
    }
    if (req.body.role) {
        item.role = req.body.role;
        req.session.account.role = item.role;
    }
    if (req.body.language) {
        item.language = req.body.language;
        req.session.account.language = item.language;
    }
    if (req.body.theme) {
        item.theme = req.body.theme;
        req.session.account.theme = item.theme;
    }

    db.Account.findOne({
        uuid: req.session.account.uuid
    }, (error, account) => {
        if (!error) {
            account.school = item.school || account.school;
            account.role = item.role || account.role;
            account.language = item.language || account.language;
            account.theme = item.theme || account.theme;
            account.updateAt = item.updateAt;
            account.ipAddress = item.ipAddress;
            account.save();
        }
    });

    res.send(req.session.account);
}

function getCode(req, res) {
    res.redirect(`${config.authUrl}?client_id=${config.clientID}&redirect_uri=${req.get('X-Forwarded-Proto') || req.protocol}://${req.hostname}${config.rootPath}/oauth/token&scope=${config.scope}&response_type=code&state=ischool_authbug_code`);
}

function getToken(req, res) {
    request({
        method: 'GET',
        uri: `${config.tokenUrl}?client_id=${config.clientID}&client_secret=${config.clientSecret}&redirect_uri=${req.get('X-Forwarded-Proto') || req.protocol}://${req.hostname}${config.rootPath}/oauth/token&code=${req.query.code}&grant_type=authorization_code`
    }, (error, response, body) => {
        var token = JSON.parse(body);
        token.expires_time = Date.now() + (token.expires_in * 1000);
        req.session.token = token;

        request({
            method: 'GET',
            uri: `${config.meUrl}?access_token=${token.access_token}&token_type=bearer`
        }, (error, response, body) => {
            var user = JSON.parse(body);

            db.Account.findOne({
                uuid: user.uuid
            }, (error, account) => {
                if (account) {
                    req.session.account = account.toJSON();
                } else {
                    var account = {
                        uuid: user.uuid,
                        firstName: user.first_name,
                        lastName: user.last_name,
                        mail: user.mail,
                        language: config.language,
                        school: {
                            current: null,
                            items: []
                        },
                        theme: config.theme
                    };

                    new db.Account(account).save();

                    req.session.account = account;
                }

                var items = [];
                user.application.forEach((item) => {
                    items.push({
                        name: item.name,
                        dsns: item.ap_name
                    })
                });

                req.session.account.school = {
                    current: items[0],
                    items: items
                };

                getSchools(req, res);
            });
        });
    });
}

function getSchools(req, res) {
    function _getSchool(item) {
        return new Promise((rs, rj) => {
            request({
                method: 'POST',
                uri: config.doorwayUrl,
                body: `<Envelope><Header><TargetService>DS.NameService.GetDoorwayURL</TargetService><SecurityToken Type="Basic"><UserName>anonymous</UserName><Password></Password></SecurityToken></Header><Body><DomainName>${item.dsns.toLowerCase()}</DomainName></Body></Envelope>`
            }, (error, response, body) => {
                convert.toJSON(body, convert.toJSONOptions, (err, result) => {
                    item.url = result && result.Envelope && result.Envelope.Body && result.Envelope.Body.DoorwayURL ? (result.Envelope.Body.DoorwayURL.$.SecuredUrl || result.Envelope.Body.DoorwayURL._) : null;
                    rs();
                });
            });
        });
    }

    var items = req.session.account.school.items;

    request({
        method: 'GET',
        uri: config.schoolUrl
    }, (error, response, body) => {
        var schools = JSON.parse(body);

        schools.Response.School.forEach((school) => {
            items.forEach((item) => {
                if (school.DSNS.toLowerCase() === item.dsns.toLowerCase()) {
                    item.name = school.SchoolName;
                    item.type = school.Type;
                    item.county = school.County;
                    item.core = school.CoreSystem;
                }
            });
        });

        Promise.all(items.map(_getSchool)).then(() => {
            items.forEach((item, i) => {
                if (!item.url) items.splice(i, 1);
            });
            items.sort(function(x, y) {
                return x.name.localeCompare(y.name);
            });

            req.session.account.school = {
                current: items[0],
                items: items
            };
            req.session.account.role = null;

            db.Account.findOne({
                uuid: req.session.account.uuid
            }, (error, account) => {
                if (!error) {
                    account.school = req.session.account.school;
                    account.role = null;
                    account.updateAt = new Date();
                    account.ipAddress = req.get("x-forwarded-for");
                    account.save();
                }
            });

            verifyRole(req, res).then(() => {
                var temp = [];
                items.forEach((item) => {
                    if (item.allowRole)
                        temp.push(item);
                });
                req.session.account.school = {
                    current: temp[0],
                    items: temp
                };
                res.redirect(config.rootPath || '/');
            });
        });
    });
}

function verifyRole(req, res) {
    function _getUserRole(item) {
        return new Promise((rs, rj) => {
            request({
                method: 'POST',
                uri: `${item.url}/basic.public`,
                body: `<Envelope><Header><TargetService>_.GetUserRole</TargetService><SecurityToken Type="PassportAccessToken"><AccessToken>${req.session.token.access_token}</AccessToken></SecurityToken></Header><Body><Account>${req.session.account.mail}</Account></Body></Envelope>`
            }, (error, response, body) => {
                convert.toJSON(body, convert.toJSONOptions, (error, result) => {
                    if (result && result.Envelope && result.Envelope.Body) {
                        item.roles = {
                            admin: result.Envelope.Body.IsSystemAdmin === 'true',
                            teacher: result.Envelope.Body.IsTeacher === 'true',
                            student: result.Envelope.Body.IsStudent === 'true',
                            parent: result.Envelope.Body.IsParent === 'true'
                        };

                        if ((config.allowRole.admin && item.roles.admin) ||
                            (config.allowRole.teacher && item.roles.teacher) ||
                            (config.allowRole.student && item.roles.student) ||
                            (config.allowRole.parent && item.roles.parent)) {
                            item.allowRole = true;

                            if (!req.session.account.school.current)
                                req.session.account.school.current = item;
                        } else {
                            item.allowRole = false;
                        }
                    }

                    rs();
                });
            });
        });
    }

    return Promise.all(req.session.account.school.items.map(_getUserRole));
}

function refreshToken(req, res) {
    return new Promise((rs, rj) => {
        request({
            method: 'GET',
            uri: `${config.tokenUrl}?client_id=${config.clientID}&client_secret=${config.clientSecret}&refresh_token=${req.session.token.refresh_token}&grant_type=refresh_token`
        }, (error, response, body) => {
            var token = JSON.parse(body);

            if (!token.error) {
                token.expires_time = Date.now() + (token.expires_in * 1000);
                req.session.token = token;
            } else
                req.session.destroy();

            rs();
        });
    });
}

function sendService(req, res) {
    if (!req.session || !req.session.token) {
        res.send({
            error: {
                Code: "Token Error",
                Message: "Token Error"
            }
        });
        return;
    }

    if (Date.now() > req.session.token.expires_time) {
        console.log(Date.now(), req.session.account.mail, req.get("x-forwarded-for"), 'expires_time - refresh_token');
        refreshToken(req, res).then(() => {
            sendService(req, res);
        });
    } else {
        console.log(Date.now(), req.session.account.mail, req.get("x-forwarded-for"), req.body.service);

        try {
            var uri = `${req.session.account.school.current.url}/${req.body.contract}`;
            var body =
                typeof(req.body.body) === "string" ? `<Body>${req.body.body}</Body>` :
                typeof(req.body.body) === "object" ? convert.toXML("Body", req.body.body, convert.toXMLOptions) : "";

            request({
                method: 'POST',
                uri: uri,
                body: `<Envelope><Header><TargetService>${req.body.service}</TargetService><SecurityToken Type="PassportAccessToken"><AccessToken>${req.session.token.access_token}</AccessToken></SecurityToken></Header>${body}</Envelope>`
            }, (error, response, body) => {
                convert.toJSON(body, convert.toJSONOptions, (error, result) => {
                    if (result.Envelope.Header && result.Envelope.Header.Status && result.Envelope.Header.Status.Code !== "0") {
                        res.send({
                            error: {
                                Code: result.Envelope.Header.Status.Code,
                                Message: result.Envelope.Header.Status.Message,
                                DSFault: result.Envelope.Header.DSFault
                            },
                            response: result.Envelope.Body
                        });
                    } else
                        res.send({
                            response: result.Envelope.Body,
                        });
                });
            });
        } catch (error) {
            console.log((new Date()).toString(), error);
            res.send({
                error: {
                    Code: "Error",
                    Message: error
                }
            });
        }
    }
}
