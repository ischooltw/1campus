var _config = {};

// its5sync
_config.its5Sync = [
    {
        dsns: 'szwc.sz.cn',
        url: 'http://221.224.79.67:8080/iTS5/service/manager/SystemManager/updateSSOdata'
    }
];

// web component 讀取 service 的 scope
_config.scope = [
    'User.Mail',
    'User.BasicInfo',
    'User.Application',
    '*:campus.core',                // 核心模組
    '*:ischool.leave.teacher',      // 線上請假模組
    '*:ischool.leave.student',      // 線上請假模組
    '*:campus.homework.teacher',    // 作業追踪模組
    '*:campus.homework.student',    // 作業追踪模組
    '*:campus.score.teacher',       // 成績模組
    '*:campus.score.student',       // 成績模組
    '*:campus.rollcall',            // 線上點名模組
    '*:campus.csp',                 // 課堂表現模組
    '*:campus.course_selection'     // 線上選課模組
].join();

// 取得 dsa 學校清單，用來比對顯示學校名稱
_config.schoolUrl = 'https://dsns.ischool.com.tw/campusman.ischool.com.tw/config.public/Get1CampusSchoolList?rsptype=json';

// 取得 dsns 的完整 url path
_config.doorwayUrl = 'https://dsns.ischool.com.tw/dsns/dsns';

// oauth 認證主機設定
_config.authServer = 'https://auth.ischool.com.tw';

_config.authUrl = `${_config.authServer}/oauth/authorize.php`;
_config.tokenUrl = `${_config.authServer}/oauth/token.php`;
_config.meUrl = `${_config.authServer}/services/me.php`;
_config.logoutUrl = `${_config.authServer}/logout.php`;

_config.clientID = 'd7eea5d78d979f37030c1aa3acfee83b';
_config.clientSecret = '51d0ccbbb04e2e3896b514d16c3422e176bf3b38a1a98153a7c59e2e556b2a67';

// 設定安裝的 url path
_config.rootPath = '';

// 預設語系
_config.language = { title: '繁體中文', code: 'zh-tw' };
// _config.language = { title: '简体中文', code: 'zh-cn' };
// _config.language = { title: 'English', code: 'en-us' };

// 預設佈景主題顏色
_config.theme = [
    'red',          //  0
    'pink',         //  1
    'purple',       //  2
    'deep-purple',  //  3
    'indigo',       //  4
    'blue',         //  5
    'cyan',         //  6
    'teal',         //  7
    'green',        //  8
    'lime',         //  9
    'amber',        // 10
    'orange',       // 11
    'deep-orange',  // 12
    'brown',        // 13
    'grey',         // 14
    'blue-grey'     // 15
][5];

// 是否載入 web2 上的 gadget： true | false
_config.loadGadget = true;

// 只顯示管理員功能： true | false
_config.coreOnly = false;

// 只顯示預設語言： true | false
_config.languageOnly = false;

// 設定顯示的角色： true | false
_config.allowRole = {
    admin: true,
    teacher: true,
    student: true,
    parent: true
};

// xml2js & js2xmlparser 的轉換設定，請勿調整!!!
_config.convert = {
    toJSON: require('xml2js').parseString,
    toJSONOptions: {
        attrkey: '$',
        charkey: '_',
        explicitArray: false
    },
    toXML: require('js2xmlparser'),
    toXMLOptions: {
        attributeString: '$',
        valueString: '_',
        declaration: {
            include: false
        }
    }
};

module.exports = _config;
